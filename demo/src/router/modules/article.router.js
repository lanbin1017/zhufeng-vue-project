export default [
    {
        path: '/post',
        name: 'post',
        component: () => import('../../views/article/Post.vue'),
        meta: {
            needLogin: false
        }
    },
    {
        path: '/demo',
        components: () => import('../../views/Home.vue'),
        // components: () => import('../../views/article/Home.vue'),
        // component: () => import('../../views/article/Post2.vue'),
        meta: {
            needLogin: false
        }
    },
]