export default [
     {
        path: '*', // 处理匹配不上的路由
        components: () => import('../../views/404.vue'),
        meta: {
            needLogin: false
        }
    },
    {
        path: '/manager',
        component: () => import(/*webpackChunkName:'manager'*/'../../views/manager/index.vue'),
        meta: {
            needLogin: false
        }
    }
]