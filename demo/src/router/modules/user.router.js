export default [
    {
        path: '/reg',
        components: () => import('../../views/user/Reg.vue'),
        meta: {
            needLogin: true
        }
    },

    {
        path: '/forget',
        components: () => import('../../views/user/Forget.vue'),
        meta: {
            needLogin: true
        }
    },

    {
        path: '/login',
        components: () => import('../../views/user/Login.vue'),
        meta: {
            needLogin: true
        }
    },

]