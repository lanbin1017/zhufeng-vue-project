import Vue from 'vue'
import VueRouter from 'vue-router'
// import hooks from './hooks'
Vue.use(VueRouter)
// 一个一个导入组件太过繁琐，建立一个动态加载的方式
//require.context()是webpack语法，创建自己的（模块）上下文。
//三个参数：搜索文件夹目录，是否搜索子目录，匹配文件的正则表达式
const files = require.context('./modules', false, /\.js$/)
const routes = []
files.keys().forEach(key => {
  routes.push(...files(key).default)
})
// 入口文件
console.log(routes, 'routes')
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  // routes//实现路由的模块化
  routes: [{
    path: '/post',
    name: 'post',
    component: () => import('./../views/article/Post.vue'),
    meta: {
      needLogin: false
    }
  },
  {
    path: '/demo',
    components: () => import('./../views/article/Demo.vue'),
    // component: () => import('./../views/article/Post2.vue'),
    meta: {
      needLogin: false
    }
  }]
})
// console.log(hooks, 'hooks')
// Object.values(hooks).forEach(hook => {
//   console.log(hook, 'hook')
//   router.beforeEach(hook.bind(router)) // 将this绑定成router
// })
export default router
