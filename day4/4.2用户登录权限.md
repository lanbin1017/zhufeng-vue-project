有关于用户管理的**配置文件**【src/api/config/user.js】**和接口文件**【src/api/user.js】在上面有如图展示，接下来把方法类型在action-types.js进行添加<br />src/store/action-types.js文件
```javascript
// 用户登录
export const USER_LOGIN = 'USER_LOGIN';

// 设置用户信息
export const SET_USER = 'SET_USER';

// 设置已经获取权限了 
export const SET_PERMISSION = 'SET_PERMISSION';
```
src/store/modules/user.js文件
```javascript
import * as user from '@/api/user';
import * as types from '../action-types';
import { setLocal, getLocal } from '@/utils/local';//token存储在本地会话中
export default {
    state: {
        userInfo: {}, // 用户信息
        hasPermission: false, // 代表用户权限
    },
    mutations: {
        [types.SET_USER](state, userInfo) {
            state.userInfo = userInfo;
            if (userInfo && userInfo.token) {//如果有token值就存储在本地会话中
                setLocal('token', userInfo.token);
            }else{
                localStorage.clear('token');
            }
        },
        [types.SET_PERMISSION](state, has) {
            state.hasPermission = has;
        }
    },
    actions: {
        async [types.SET_USER] ({commit},{payload,permission}){
            commit(types.SET_USER, payload);
            commit(types.SET_PERMISSION, permission)
        },
        async [types.USER_LOGIN]({ commit ,dispatch}, payload) {
          //payload --- 用户信息
            try {
                let result = await user.login(payload);
                dispatch(types.SET_USER,{payload:result.data,permission:true})
            } catch (e) {
                return Promise.reject(e);
            }
        }
    }
}
```
src/views/user/Login.vue文件
```vue
<script>
import { v4 } from "uuid";//uuid中有v4方法，产生不同的字符串
import { getLocal, setLocal } from "@/utils/local.js";
import { getCaptcha } from "@/api/public";//获取的是验证码的接口
// 命名空间的使用,函数处理化，传入user后传出的是mapActions，针对的就是user目录 --- user/login.vue
import {createNamespacedHelpers} from  'vuex';
let {mapActions} = createNamespacedHelpers('user');
import * as types from '../../store/action-types'
export default {
 //...
  methods: {
    ...mapActions([types.USER_LOGIN]),
    submitForm(formName) {
      this.$refs[formName].validate(async valid => {
        if (valid) {
          try{//传入表格信息和验证码
            this[types.USER_LOGIN]({...this.ruleForm,uid:this.uid});
          }catch(e){
            this.$message({type:'error',message:e})
          }
        } else {
          return false;
        }
      });
    },
//...
</script>  
```
验证是否登录，如果登陆过就保存记录并且刷新的时候不会页面和信息更新。<br />修改下页面，src/components/PageHeader.vue文件
```vue
//...
 <template v-if="!hasPermission">
     <el-menu-item index="login">
       <router-link to="/login">登录</router-link>
     </el-menu-item>
     <el-menu-item index="reg">
       <router-link to="/reg">注册</router-link>
     </el-menu-item>
   </template>
   <el-submenu index="profile" v-else>
     <template slot="title">{{userInfo.username}}</template>
     <el-menu-item @click="$router.push('/manager')">管理后台</el-menu-item>
     <el-menu-item index="logout" @click="logout">退出登录</el-menu-item>
   </el-submenu>
//...
<script>
import { createNamespacedHelpers } from "vuex";
let { mapState,mapActions} = createNamespacedHelpers("user");
import * as types from '../store/action-types'
export default {
  computed: {
    ...mapState(["hasPermission", "userInfo"])
  },
  methods:{
    logout(){//在下文中可以实现这个方法
      alert("退出成功！")
    } 
  }
};
</script>

```
数据接口要得到验证<br />src/utils/request.js文件
```javascript
import {getLocal} from '@/utils/local'
//...
setInterceptors(instance) {
        instance.interceptors.request.use(config => {
            // 一般增加一些token属性等  jwt
            config.headers.authorization = 'Bearer ' + getLocal('token')
            return config;
        });
 //...
```
**验证是否登录的接口:**[**http://vue.zhufengpeixun.cn**](http://vue.zhufengpeixun.cn/)**/user/validate**<br />继续在action-types.js添加方法<br />src/store/action-types.js文件
```javascript
// 验证是否登录过
export const USER_VALIDATE = 'USER_VALIDATE';
```
继续在src/store/modules/user.js文件
```javascript
import * as user from '@/api/user';
import * as types from '../action-types';
import { setLocal, getLocal } from '@/utils/local';
export default {
//...
    actions: {
//...
        async [types.USER_VALIDATE]({dispatch}){
            // 如果没token 就不用发请求了 肯定没登录
            if(!getLocal('token')) return false;
            try{
                let result =  await user.validate();
                dispatch(types.SET_USER,{payload:result.data,permission:true})
                return true;
            }catch(e){  
                dispatch(types.SET_USER,{payload:{},permission:false});
                return false;
            }
        },
    }
}
```
要是想要完成这个效果，需要的路由的钩子函数进行验证，下文就会讲到。